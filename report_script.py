import os
from datetime import date, timedelta

import pyminizip
import slack
from slack.errors import SlackApiError

SLACK_TOKEN = "replace-with-real-token"
SLACK_CHANNEL = "reports"
COMPRESSION_LEVEL = 5
ZIP_PASSWORD = "randompassword"
ZIP_FILENAME = f'./dk-report-{(date.today() - timedelta(1)).strftime("%d_%m_%Y")}.zip'
CSV_FILE = "csv/email-password-recovery-code.csv"

slack_client = slack.WebClient(token=SLACK_TOKEN)

if __name__ == "__main__":
    try:
        csvPath = os.path.join(os.getcwd(), CSV_FILE)
        pyminizip.compress(
            csvPath,
            None,
            ZIP_FILENAME,
            ZIP_PASSWORD,
            COMPRESSION_LEVEL,
        )
    except FileNotFoundError:
        print("Directory: {0} does not exist".format(csvPath))
    except NotADirectoryError:
        print("{0} is not a directory".format(csvPath))
    except PermissionError:
        print("You do not have permissions to change to {0}".format(csvPath))

    try:
        response = slack_client.files_upload(
            file=ZIP_FILENAME,
            filetype="zip",
            title=f'Daily report {(date.today() - timedelta(1)).strftime("%d_%m_%Y")}',
            channels=SLACK_CHANNEL,
        )
    except SlackApiError as e:
        assert e.response["ok"] is False
        assert e.response["error"]
        print(f"Error while uploading file to the Slack channel: {e.response['error']}")
